package comparation

import "sync"

type Switcher struct {
	mu    *sync.Mutex
	state bool
}

func (s *Switcher) On() bool {
	s.mu.Lock()
	defer s.mu.Unlock()

	if s.state {
		return true
	}

	s.state = true

	return false
}

func (s *Switcher) Off() {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.state = false
}

func NewSwitcher(state bool) *Switcher {
	return &Switcher{
		mu:    new(sync.Mutex),
		state: state,
	}
}
