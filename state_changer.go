package comparation

type StateChanger interface {
	ChangeStateSlow()
	ChangeStateFast()
}
