package comparation

import (
	"sync/atomic"
	"time"
)

type ConditionService struct {
	switcher   *Switcher
	fast, slow int64
}

func (s *ConditionService) Do(wg WaitGroup) {
	if s.switcher.On() {
		s.doFast(wg)
	} else {
		s.doSlow(wg)
	}
}

func (s *ConditionService) Fast() int64 {
	return s.fast
}

func (s *ConditionService) Slow() int64 {
	return s.slow
}

func (s *ConditionService) doFast(wg WaitGroup) {
	atomic.AddInt64(&s.fast, 1)

	wg.Done()
}

func (s *ConditionService) doSlow(wg WaitGroup) {
	defer s.switcher.Off()

	atomic.AddInt64(&s.slow, 1)

	time.Sleep(Delay)

	wg.Done()
}

func NewConditionService() Service {
	return &ConditionService{
		switcher: NewSwitcher(false),
	}
}
