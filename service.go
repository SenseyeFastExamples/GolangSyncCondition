package comparation

import "time"

const Delay = 100 * time.Millisecond

type WaitGroup interface {
	Done()
}

type Service interface {
	Do(WaitGroup)
	Fast() int64
	Slow() int64
}
