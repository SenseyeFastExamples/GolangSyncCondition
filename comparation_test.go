package comparation

import (
	"sync"
	"testing"
)

func BenchmarkConditionService(b *testing.B) {
	benchmarkService(b, NewConditionService())
}

func BenchmarkStateService(b *testing.B) {
	benchmarkService(b, NewStateService())
}

func benchmarkService(b *testing.B, service Service) {
	wg := new(sync.WaitGroup)
	wg.Add(b.N)

	for i := 0; i < b.N; i++ {
		go service.Do(wg)
	}

	wg.Wait()

	b.Logf("Fast: %d", service.Fast())
	b.Logf("Slow: %d", service.Slow())
}
