package comparation

import (
	"sync/atomic"
	"time"
)

type (
	StateAction interface {
		Do(WaitGroup)
		Count() int64
	}

	FastStateAction struct {
		count int64
	}

	SlowStateAction struct {
		count        int64
		stateChanger StateChanger
	}
)

func (a *FastStateAction) Do(wg WaitGroup) {
	atomic.AddInt64(&a.count, 1)

	wg.Done()
}

func (a *FastStateAction) Count() int64 {
	return a.count
}

func (a *SlowStateAction) Do(wg WaitGroup) {
	a.stateChanger.ChangeStateFast()

	atomic.AddInt64(&a.count, 1)

	time.Sleep(Delay)

	a.stateChanger.ChangeStateSlow()

	wg.Done()
}

func (a *SlowStateAction) Count() int64 {
	return a.count
}

func NewSlowStateAction(stateChanger StateChanger) StateAction {
	return &SlowStateAction{
		stateChanger: stateChanger,
	}
}

func NewFastStateAction(stateChanger StateChanger) StateAction {
	return &FastStateAction{}
}
