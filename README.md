# GolangSyncCondition

##### Compare change strategy between slow & fast
```bash
go test . -bench=. -benchmem
```

* when use all time mutex:
```text
BenchmarkConditionService:
Delay = 100 * time.Nanosecond
1000000              3200 ns/op             475 B/op          1 allocs/op
Delay = 1 * time.Millisecond
3000000               520 ns/op              19 B/op          0 allocs/op
Delay = 10 * time.Millisecond
3000000               547 ns/op              25 B/op          0 allocs/op
Delay = 100 * time.Millisecond
1000000              2144 ns/op             375 B/op          0 allocs/op
```

* when use mutex only to change strategy
```text
BenchmarkStateService
Delay = 100 * time.Nanosecond
exit status 2
Delay = 1 * time.Millisecond
5000000               302 ns/op               0 B/op          0 allocs/op
Delay = 10 * time.Millisecond
5000000               304 ns/op               0 B/op          0 allocs/op
Delay = 100 * time.Millisecond
5000000               320 ns/op               0 B/op          0 allocs/op
```

##### Conclusions:
When operation is slow -> use state, when fast -> state is overhead