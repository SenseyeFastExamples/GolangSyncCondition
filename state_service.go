package comparation

import (
	"sync"
)

type StateService struct {
	mu                  *sync.Mutex
	current, fast, slow StateAction
}

func (s *StateService) Do(wg WaitGroup) {
	s.current.Do(wg)
}

func (s *StateService) Fast() int64 {
	return s.fast.Count()
}

func (s *StateService) Slow() int64 {
	return s.slow.Count()
}

func (s *StateService) ChangeStateSlow() {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.current = s.slow
}

func (s *StateService) ChangeStateFast() {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.current = s.fast
}

func NewStateService() Service {
	service := &StateService{
		mu: new(sync.Mutex),
	}

	service.fast = NewFastStateAction(service)
	service.slow = NewSlowStateAction(service)
	service.current = service.slow

	return service
}
